export class Inventory {

    itemId:number;
    partNo: string;
    description: string;
    vendorName: string;
    vendorPOC: string;
    vendorMobile: string;
    vendorAddress: string;
    vendorCity: string;
    vendorState: string;
    vendorCountry: string;
    quantity: string;
    isInflamable: boolean;
    
    constructor() { }
}
