import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Inventory } from './Inventory';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  
  baseUrl: string = "http://ec2-13-229-209-129.ap-southeast-1.compute.amazonaws.com:8080/api";

  constructor(private httpClient: HttpClient) { }

  // get calls
  get_inventory() {
    return this.httpClient.get(this.baseUrl + '/inventory/all');
  }
  // post calls

  add_inventory(inventory: Inventory) {
    console.log(inventory.itemId);
    console.log(inventory.description);
    return this.httpClient.post(this.baseUrl + '/inventory/create/', inventory);
  }

  update_inventory(inventory: Inventory) {
    console.log("update"+inventory.itemId);
    return this.httpClient.post(this.baseUrl + '/inventory/update/', inventory);
  }

  delete_inventory(id:any) {
    console.log("id"+id);
    return this.httpClient.post(this.baseUrl + '/inventory/delete/'+id, httpOptions);
  }
}
