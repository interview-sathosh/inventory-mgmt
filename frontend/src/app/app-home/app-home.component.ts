import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service'
import { Inventory } from 'src/app/Inventory';


export interface tableData {
  partNo: string;
  itemId: number;
  description: string;
  vendorName: string;
  vendorMobile: string;
  vendorCountry: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './app-home.component.html',
  styles: [``]
})
export class AppHomeComponent implements OnInit {

  constructor(private service: DataService) { }
  private dataLoaded: boolean;
  private tableData: any
  private yourMessage = []

  ngOnInit() {

    this.get();

  }

  private get() {
    this.service.get_inventory().subscribe(data => {
      this.tableData = data;
      this.dataLoaded = true;
    });
  }
  private create(inventory: Inventory) {
    console.log(inventory.itemId);
    console.log(inventory.description);
    this.service.add_inventory(inventory).subscribe(data => {
      this.get(); 
      
    });
  }
  private update(inventory: Inventory) {
    this.service.update_inventory(inventory).subscribe(data => {
     
    });
  }
  private delete(itemId: number) {
    console.log("compinened id:"+itemId);
    this.service.delete_inventory(itemId).subscribe(data => {
     
    });
  }

  private action(row: any) {
    console.log(row.operation);
    if (row.operation === 'added') {
      console.log(row.itemId);
      console.log(row.description);
      this.create(row);
      this.yourMessage.push('success', row.partNo + ' has been added successfully!')
      this.yourMessage = []
    }
    if (row.operation === 'updated') {
      this.update(row);
      this.yourMessage.push('success', row.partNo + ' has been updated successfully!')
      this.yourMessage = []
    }
    if (row.operation === 'deleted') {
      this.delete(row.itemId);
      this.yourMessage.push('success', ' has been deleted successfully!')
      this.yourMessage = []
    }

  }
}