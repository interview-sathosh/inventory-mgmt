# Inventory Management 
Stack of technologies
## Back End
	-Spring Boot
	-Maven 	
	-Java
##UI Layer
	-Angular
DB - MySql

Local Setup steps

UI Layer

Make sure you have Node.js installed.

$ git clone or download 
$ cd frontend 
$ npm install $ npm start

Inventory Management should be running on port 4200

Back End

$ cd API 
$ mvn clean install
$ mvn spring-boot:run

Make sure Back end is started first.

Author : Santhosh