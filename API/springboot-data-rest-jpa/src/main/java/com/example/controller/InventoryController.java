
package com.example.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Inventory;
import com.example.repository.InventoryRepository;

/**
 * @author Santhosh
 *
 */

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/inventory")
public class InventoryController {
	private Logger LOGGER = Logger.getLogger(InventoryController.class.getName());


	@Autowired
	InventoryRepository invetoryRepsitory;
	
	@GetMapping("/test")
	public String test() {
		LOGGER.info("get all customers");
		return "REST API test success!";
	}
	
	@GetMapping("/all")
	public List<Inventory> get() {
		LOGGER.info("get all customers");
		return invetoryRepsitory.findAll();
	}

	@PostMapping("/create")
	public Inventory create(@Valid @RequestBody Inventory customer) {
		System.out.print(customer.getPartNo());
		System.out.print(customer.getItemId());
		return invetoryRepsitory.save(customer);
	}
	
	@PostMapping("/update")
	public Inventory update(@Valid @RequestBody Inventory customerUpdate) {
		Inventory customerFind = invetoryRepsitory.findOne(customerUpdate.getItemId());
		customerFind.setPartNo(customerUpdate.getPartNo());
		customerFind.setDescription(customerUpdate.getDescription());
		customerFind.setVendorName(customerUpdate.getVendorName());
		return invetoryRepsitory.save(customerFind);
	}
	
	@PostMapping("/delete/{id}")
	public void delete(@PathVariable(value = "id") Long customerId) {
		try {
		invetoryRepsitory.delete(customerId);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Inventory> findById(@PathVariable(value = "id") Long customerId) {
		Inventory customer = invetoryRepsitory.findOne(customerId);
		if(customer == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(customer);
	}
	

}
